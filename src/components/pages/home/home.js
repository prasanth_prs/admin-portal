import React from 'react';
import {Tabs, Tab,  Row,  Col} from "react-bootstrap";


import OrderLander from '../order/orderLander';
import AdminProfile from '../profile/adminProfile';
import ProductLander from '../product/prodLander';

function Home() {


  return (
    <div className=''>
    <Row>
        <Col>
            <Tabs defaultActiveKey="order" 
                  id="controlled-tab-example">
                <Tab eventKey="order" title="Order">
                    <OrderLander />
                </Tab>
                <Tab eventKey="profile" title="Profile">
                    <AdminProfile />
                </Tab>
                <Tab eventKey="product" title="Product" >
                    <ProductLander />
                </Tab>
            </Tabs>
        </Col>
    </Row>
</div>
  );
}

export default Home;
