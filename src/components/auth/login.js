import React, { useState } from "react"
import { useHistory } from "react-router-dom"


export default function Login (){

    const [input, setInput] = useState({username:'',password:''})
    const history = useHistory()

    const handlechange = e => {
        setInput({...input,[e.target.name]: e.target.value})
      }

    const handleLogin =()=>{
      if(input.username === 'adminprs' && input.password==='a')
      {
        history.push('/admin')
      }else{
          document.getElementById('err').innerHTML='Wrong'
        history.push('/')
      }
    }

    return(
        <div className="container">
        <div className="row justify-content-center align-items-center">
            <div className="col-lg-6 col-md-6 col-sm-6 loginBox">
                <div className="col loginKey">
                    <i className="fa fa-key" aria-hidden="true"></i>
                </div>
                <div className="col loginTitle">
                    ADMIN PANEL
                </div>
                <div className="col loginForm">
                    <div className="form-group">
                        <label className="inputLabel">USERNAME</label>
                        <input type="text" className="form-control loginInput" name='username' onChange={handlechange} value={input.username}/>
                    </div>
                    <div className="form-group">
                        <label className="inputLabel">PASSWORD</label>
                        <input type="password" className="form-control loginInput" name='password' onChange={handlechange} value={input.password}/>
                    </div>
                    <p id='err' className='text-danger'></p>
                    <div className="col loginButton text-right">
                        <button type="button" className="btn" onClick={handleLogin}>LOGIN</button>
                    </div>
                </div>
                <div className="col forgetLink ">
                    <a className='btn' href='/'>Forget Password ?</a>
                </div>
            </div>
        </div>
    </div>
    )
}