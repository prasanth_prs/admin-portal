import React from 'react';
import './style.css';
import { Route, Switch} from "react-router-dom";
import {Navbar, Nav} from 'react-bootstrap'

import NavBar from '../commen/navBar';
import Home from './home/home';
import OrderLander from './order/orderLander'
import ProductLander from './product/prodLander'
import AdminProfile from './profile/adminProfile'
import UserLander from './user/userLander'

function MainLander() {


  return (
    <section className="py-3 ">
    <div className="container-fluid">
    <NavBar></NavBar>
        <div className="bg-white rounded-lg d-block d-flex">
            {/* -------Sidebar-------- */}
            <div id="sidebar" className="shadow rounded-lg">
                <Navbar expand="lg" className="sidebar border-bottom mt-4">
                <Navbar.Toggle  aria-controls = "navbar_toggle"/>
                    <Navbar.Collapse id = "navbar_toggle"> 
                        <Nav className="m-auto flex-column sidebar-nav" activeKey={window.location.pathname}>
                            <Nav.Link   href="/admin" className='pill'><i className="fa fa-desktop fa-lg text-primary m-2"></i>Home</Nav.Link>
                            <Nav.Link   href="/admin/product" className='pill'><i className="fa fa-cog fa-lg text-primary m-2"></i> Product</Nav.Link>

                            <Nav.Link disabled className='head'>Manage Orders</Nav.Link>
                            <Nav.Link   href="/admin/order" className='pill'><i className="fa fa-list fa-lg text-success m-2"></i> Orders</Nav.Link>
                            <Nav.Link   href="/admin/order" className='pill'><i className="fa fa-table fa-lg text-info m-2"></i> View</Nav.Link>

                            <Nav.Link disabled className=' head'>Manage User</Nav.Link>
                            <Nav.Link   href="/admin/user" className='pill'><i className="fa fa-user-plus fa-lg text-success m-2"></i> Create</Nav.Link>
                            <Nav.Link   href="/user" className='pill'><i className="fa fa-user-o fa-lg text-info m-2"></i> Edit</Nav.Link>
                            <Nav.Link   href="/user" className='pill'><i className="fa fa-users fa-lg text-primary m-2"></i> View</Nav.Link>

                            <Nav.Link disabled className='head'></Nav.Link>
                            <Nav.Link   href="/admin/profile" className='pill'><i className="fa fa-cogs fa-lg text-warning mt-2"></i> Settings</Nav.Link>
                            <Nav.Link className='pill'><i className="fa fa-sign-out fa-lg text-danger mt-2"></i> Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>

            {/* -------Content body-------- */}
            <div className="tab-content p-2 p-md-2">
                <Switch>
                    <Route exact path="/admin" component={Home}/>
                    <Route exact path="/admin/product" component={ProductLander}/>
                    <Route exact path="/admin/order" component={OrderLander}/>
                    <Route exact path="/admin/user" component={UserLander}/>
                    <Route exact path="/admin/profile" component={AdminProfile}/>
                </Switch>
            </div>
        </div>
    </div>
</section>
  );
}

export default MainLander;
